import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { KeycloakService } from '../auth/keycloack/keycloack.service';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'cabinet',
    templateUrl: './capinet.component.html',
})
export class CabinetComponent implements OnInit {

    profile: User;
   // contracts: Contract[];

    constructor(private keycloakService: KeycloakService,private http:HttpClient
        ) {
    }

    public ngOnInit(): void {
        this.http.get('http://localhost:9090/user/cabinet').subscribe((data:User) => this.profile=data)
    }
  
    // public isManager(): boolean {
    //     return this.keycloakService.hasAnyRole(['manager']);
    // }

    // public isAdmin(): boolean {
    //     return this.keycloakService.hasAnyRole(['admin']);
    // }

    // public getContracts() {
    //     this.contractService.getContracts().subscribe(
    //         data => {
    //             this.contracts = data;
    //         }
    //     );
    // }

    public logout() {
        this.keycloakService.logout();
    }

    Role():boolean{
       let a= this.keycloakService.role();
       return a;
    }
}