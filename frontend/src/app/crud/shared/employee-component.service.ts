import { Injectable, OnInit } from '@angular/core';
import { EmployeeComponent } from './employee-component.model';
import { HttpClient, HttpEventType, HttpParams, HttpRequest, HttpHeaders} from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { TokenStorageService } from 'src/app/auth/token-storage.service';



const TOKEN_HEADER_KEY = 'Authorization';
@Injectable({
  providedIn: 'root'
})
export class EmployeeComponentService {

 Employees: EmployeeComponent[];
 Employee:EmployeeComponent;
 aaa:EmployeeComponent[];
  private URL:string="http://localhost:9090/api/";
  

  constructor(private http:HttpClient,private toastr:ToastrService,
              private cookie:CookieService,private token:TokenStorageService,) { 
     

  }

//req: HttpRequest<any>
  getUsers(){
    let authReq =new HttpParams();
    
    const token = this.token.getToken();
    // if (token != null) {
    //    // authReq = this.req.clone();
    //    this.cookie.set('JSESSIONID','Token'+token);
    // }//authReq.append(TOKEN_HEADER_KEY, 'Bearer ' + token);
    // //authReq.
    console.log(authReq);
    
    this.http.get(this.URL+"getEmployees").subscribe((data:EmployeeComponent[]) => this.Employees=data);
    
    //{}headers: this.req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token), ,,

  }

  postUser(){
  this.aaa=this.Employees;
    this.http.post(this.URL+"createEmployee",this.Employee).subscribe(()=>{
      this.getUsers();
          this.toastr.success('New user created!','Successful.');
    });
  }

putUser(){
  this.http.put(this.URL+"edit/"+this.Employee.id,this.Employee, { withCredentials: true}).subscribe(()=>{
    this.getUsers();
   
        this.toastr.success('User edit', 'Successful.');
    
  });
}

  deleteUser(){
    
    this.http.delete(this.URL+"employees/"+this.Employee.id).subscribe(()=>{
      this.getUsers();
     
       
          this.toastr.success('User delete!', 'Successful.');
      
    });
  }

  googleAuth(){
    
    this.http.post(this.URL+"login",null);
   
  }

  logout(){
    //this.http.get('http://localhost:8080/auth/realms/Angular/protocol/openid-connect/logout').subscribe();
    this.http.get("http://localhost:9090/logout").subscribe();
  }

  sortByName(){
    
    this.http.get(this.URL+"getEmployeesOrderBy/name" ,{withCredentials: true}).subscribe((data:EmployeeComponent[]) => this.Employees=data);
    
  }

 
}
  



