import { Injectable } from '@angular/core';

import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { ReportModel } from './reportmodel';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
  })
  export class ReportComponentService {
  
 
    private URL:string="http://localhost:9090/file/";
    Reports: ReportModel[]
    //private sss:string=this.cookie.get('JSESSIONID').toString();
  
    constructor(private http:HttpClient,private toastr:ToastrService,
                private cookie:CookieService,private token:TokenStorageService,) { 
       
  
    }
  
  //req: HttpRequest<any>
    getReports(){
      // let authReq =new HttpParams();
      
      // const token = this.token.getToken();
      // if (token != null) {
      //    // authReq = this.req.clone();
         
      // }//authReq.append(TOKEN_HEADER_KEY, 'Bearer ' + token);
      // //authReq.
      // console.log(authReq);
      // this.cookie.set('JSESSIONID','Token'+token);
      this.http.get(this.URL+"magazine",{withCredentials: true}).subscribe((data:ReportModel[]) => this.Reports=data);
      
      //{}headers: this.req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token), ,,
  
    }
}