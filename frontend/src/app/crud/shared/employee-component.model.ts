

export class EmployeeComponent {
    id :number;
    name :string;
    surname :string;
    patronymic :string;
    status :string;
    position :string;
    photoPath :string ;
}
