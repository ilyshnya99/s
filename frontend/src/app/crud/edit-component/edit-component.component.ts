import { Component, OnInit } from '@angular/core';
import { EmployeeComponent } from '../shared/employee-component.model';
import { EmployeeComponentService } from '../shared/employee-component.service';
import { Router } from '@angular/router';
import { TableComponent } from '../table/table.component';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-edit-component',
  templateUrl: './edit-component.component.html',
  styleUrls: ['./edit-component.component.css'],
  providers: [EmployeeComponentService]
})
export class EditComponentComponent implements OnInit {

  emp:EmployeeComponent[];
  Emp:EmployeeComponent=new EmployeeComponent();
  Empl:any
constructor(private httpService: EmployeeComponentService,private router:Router,private toastr:ToastrService){


  }
  Back(){
    this.router.navigate(['home']);
  }
  ngOnInit(){
   
    this.Empl=TableComponent.Ident;
    // this.httpService.getUsers();
     this.httpService.Employee=
     {
       id:0,
       name:this.Empl.name,
       surname:this.Empl.surname,
       position:this.Empl.position,
       status:this.Empl.status,
       photoPath:this.Empl.photoPath,
       patronymic:this.Empl.patronymic
     }
  }

  populateForm(pd:any) {
   this.httpService.Employee.id = pd.id;
    }

 Edit(){
   
     this.populateForm(TableComponent.Ident);
    this.httpService.putUser();
    
  }

  showSuccess() {
    this.toastr.success('Hello world!', 'Toastr fun!');
  }


}
