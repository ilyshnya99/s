import { Component, OnInit ,OnChanges} from '@angular/core';
import { EmployeeComponent } from '../shared/employee-component.model';
import { EmployeeComponentService } from '../shared/employee-component.service';
import { DomSanitizer } from '@angular/platform-browser';

import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { KeycloakService } from 'src/app/auth/keycloack/keycloack.service';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  providers: [EmployeeComponentService]
})
export class TableComponent implements OnInit {
  emp:EmployeeComponent[];
   Emp:EmployeeComponent=new EmployeeComponent();
  static Ident:EmployeeComponent; 
constructor(private httpService: EmployeeComponentService,private router:Router,
             private cookie:CookieService,public sanitizer: DomSanitizer,
             private keycloak:KeycloakService){

  }
  goHome(){
         
    this.router.navigate(['home/add']);
  }
  ngOnInit(){
   
     this.httpService.getUsers();
     this.httpService.Employee=
     {
       id:0,
       name:"",
       surname:"",
       position:"",
       status:"",
       photoPath:"",
       patronymic:""
     }
  }
 
  populateForm(pd:any) {
   this.httpService.Employee.id = pd.id;
    }

  Delete(pd:any){
    if(confirm("Delete Employee ?"))
    {
      this.populateForm(pd);
      this.httpService.deleteUser();
    }
  }

  ReEdit(pd:any){
    TableComponent.Ident=pd;
    this.router.navigate(['home/edit']);
  }
 
  Upload(){
    this.router.navigate(['home/upload']);
  }
  onLogout() {
    this.keycloak.logout();
    localStorage.clear();
    this.cookie.delete("AUTH_SESSION_ID");
    this.cookie.deleteAll('/auth/realms/Angular/','localhost');
    //this.httpService.logout();
    //this.router.navigate(['/']);
  }

  
public getSantizeUrl(url : string) {
  return this.sanitizer.bypassSecurityTrustUrl(url);
}

  Sort(){
    this.httpService.sortByName();
  }

  Magazine(){
    this.router.navigate(['home/magazine']);
  }
}
