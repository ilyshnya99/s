
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { TableComponent } from './crud/table/table.component';
import { AddComponentComponent } from './crud/add-component/add-component.component';
import { EditComponentComponent } from './crud/edit-component/edit-component.component';
import { AuthGuard } from './auth/auth.guard';
import { SimpleDemoComponent } from './file-upload.component/file-upload.component';
import { ReportComponent } from './report/report-component';
import { CabinetComponent } from './cabinet/capinet.component';
import { FlagComponent } from './flag/flag-component';

const routes: Routes = [
 // {path:'',redirectTo:'/user/login',pathMatch:'full'},
  // {
  //   path: 'user', component: UserComponent,
  //   children: [
  //     { path: 'registration', component: RegistrationComponent },
  //     { path: 'login', component: LoginComponent }
  //   ]
  // },
 
  {path:'home',component:TableComponent,canActivate: [AuthGuard]},
  {path:"home/add",component:AddComponentComponent},
  {path:"home/edit",component:EditComponentComponent,canActivate: [AuthGuard]},
  {path:"home/upload",component:SimpleDemoComponent,canActivate: [AuthGuard]},
  {path:"home/magazine",component:ReportComponent,canActivate: [AuthGuard]},
  {path:"home/cabinet",component:CabinetComponent},
  {path:"",component:CabinetComponent},
  {path:"home/flags",component:FlagComponent}


];

const route: Routes = [
 {
    path: '', component:TableComponent,
   
  },
  { path:'add',component:AddComponentComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
