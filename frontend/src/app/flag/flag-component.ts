import { Component, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';

import { Flag} from "./flag";
import { KeycloakService } from '../auth/keycloack/keycloack.service';

import {NgxPaginationModule} from 'ngx-pagination';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
    selector: 'flag',
    templateUrl: './flag-component.html',
    styleUrls: ['./flag-component.css']
})
export class FlagComponent implements OnInit,OnChanges {
 
    buyTicketForm: FormGroup;
    name:string="";
    flag: Flag;
   // contracts: Contract[];
    flags:Flag[];

    config: any;
    collection = { count: 60, data: [] };
   
    constructor(private keycloakService: KeycloakService,private http:HttpClient) {
 
       this._createForm();
    }



    ngOnChanges(): void {
        // this.http.get('http://localhost:9090/country/name/'+this.name).subscribe((data:Flag[]) =>   { this.flags=data
        // this.config = {
        //     itemsPerPage: 9,
        //     currentPage: 1,
        //     totalItems: data.length
        //   };})
          this.buyTicketForm.get('passenger').valueChanges.subscribe(v => {
              if(v.length==0){
                this.http.get('http://localhost:9090/country/all').subscribe((data:Flag[]) => { this.flags=data
                this.config = {
                    itemsPerPage: 9,
                    currentPage: 1,
                    totalItems: data.length
                  };})
              }
              else{ 
                this.http.get('http://localhost:9090/country/name/'+v).subscribe((data:Flag[]) => { this.flags=data
                this.config = {
                    itemsPerPage: 9,
                    currentPage: 1,
                    totalItems: data.length
                  };}) 
          }
        })
        
    }
    public ngOnInit(): void {
        this.http.get('http://localhost:9090/country/all').subscribe((data:Flag[]) => { this.flags=data
        this.config = {
            itemsPerPage: 9,
            currentPage: 1,
            totalItems: data.length
          };})
    }
  
    pageChanged(event){
        this.config.currentPage = event;
    }

    private _createForm() {
        this.buyTicketForm = new FormGroup({
          passenger: new FormControl(null)
        })
      }


    // public logout() {
    //     this.keycloakService.logout();
    // }

    // Role():boolean{
    //    let a= this.keycloakService.role();
    //    return a;
    // 
}
