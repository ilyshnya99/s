
import { Component, OnInit } from '@angular/core';
import { FileUploader,FileDropDirective,FileSelectDirective } from 'ng2-file-upload';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

// const URL = '/api/';
const URL = 'http://localhost:9090/file/upload';
 
@Component({
  selector: 'simple-demo',
  templateUrl: './file-upload.component.html'
})
export class SimpleDemoComponent{//implements OnInit{

 
  uploader:FileUploader;
  hasBaseDropZoneOver:boolean;
  hasAnotherDropZoneOver:boolean;
  response:string;
  u:FileUploader;
 
  constructor (private toastr:ToastrService,private router:Router){
    
   // let header=new Headers();
  //  header.set('content-type','application/json');
    this.uploader = new FileUploader({
     // allowedFileType:'application/json',
      url: URL,
      
      //disableMultipart: true, // 'DisableMultipart' must be 'true' for formatDataFunction to be called.
     //formatDataFunctionIsAsync: true,
      formatDataFunction: async (item) => {
        return new Promise( (resolve, reject) => {
          resolve({
            name: item._file.name,
            length: item._file.size,
            contentType: item.file.type,
            date: new Date()
          });
        
        });
        
      }
    });

    this.hasBaseDropZoneOver = false;
    this.hasAnotherDropZoneOver = false;
    
    this.response = '';
 
    this.uploader.response.subscribe( res => {this.response = res
      this.toastr.success(this.response, 'Successful.');
    });
   
  }
  Back(){
    this.router.navigate(['home']);
  }
 
  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }
 
  public fileOverAnother(e:any):void {
    this.hasAnotherDropZoneOver = e;
  }

  responseClear(){
    this.response="";
  }
}