import { Injectable } from '@angular/core';

import { HttpClient} from '@angular/common/http';
declare var Keycloak: any;

@Injectable({
  providedIn: 'root'
})
export class KeycloakService {

  private keycloakAuth: any;
  static auth: any = {};
  constructor(private http:HttpClient) { }

  init(): Promise<any> {
    return new Promise((resolve, reject) => {
      const config = {
        'url': 'http://localhost:8080/auth',
        'realm': 'Angular',
        'clientId': 'angular'
      };
      this.keycloakAuth = new Keycloak(config);
      this.keycloakAuth.init({ onLoad: 'login-required' })
        .success(() => {
          resolve();
        })
        .error(() => {
          reject();
        });
    });
   
  }

  getToken(): string {
    
    return this.keycloakAuth.token;
    
  }

  logout(){
    window.localStorage.clear();this.http.get("http://localhost:9090/logout").subscribe();
    window.sessionStorage.clear();
    this.keycloakAuth.logout({redirectUri:'http://localhost:4200/home/cabinet'});
    
 //  this.keycloakAuth=null;
  //  KeycloakService.auth.authz.logout( { redirectUri: "http://localhost:4200" } );
    //KeycloakService.auth.loggedIn = false;
  //  KeycloakService.auth.authz = null;
     }

     role():boolean {
      return this.keycloakAuth.hasRealmRole("admin");
       }


}