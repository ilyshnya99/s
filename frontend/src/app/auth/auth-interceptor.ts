import { HTTP_INTERCEPTORS, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

import { TokenStorageService } from './token-storage.service';
import { KeycloakService } from './keycloack/keycloack.service';
import { Observable } from 'rxjs';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

//    constructor(private token: TokenStorageService) { }


    constructor(private kcService: KeycloakService) {}
intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authToken = this.kcService.getToken() || '';
    request = request.clone({
      setHeaders: {
        'Authorization': 'Bearer ' + authToken
      }
    });
    return next.handle(request);
  }
//intercept(req: HttpRequest<any>, next: HttpHandler) {
//         let authReq = req;
//         const token = this.token.getToken();
//         if (token != null) {
//            // authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token)});
           
//         }
        
//      //   authReq.headers.set('Access-Control-Allow-Origin', 'http://localhost:8080/');
// //
//        // authReq.headers.append('Access-Control-Allow-Origin', '*');
//      //  authReq.headers.append('Access-Control-Allow-Methods', 'GET, POST, PUT,OPTIONS');
//    // headers.append('Access-Control-Allow-Origin', 'http://localhost:3000');

        
//         //headers.append('GET', 'POST', 'OPTIONS');
//             //authReq.headers.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
//         return next.handle(req);
//     }
// }
}
export const httpInterceptorProviders = [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
];

