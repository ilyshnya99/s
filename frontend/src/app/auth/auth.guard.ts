import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tokenKey } from '@angular/core/src/view';
import { CookieService } from 'ngx-cookie-service';
import { KeycloakService } from './keycloack/keycloack.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {


  constructor(private router: Router,private cookie:CookieService,private key:KeycloakService) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    // if (sessionStorage.getItem("AuthToken") != null || this.cookie.get('JSESSIONID')!="Tokennull"){
   
    //   return true;}
    if(this.key.role()){
      return true;
    }
    else {
      this.router.navigate(['/home/cabinet']);
      return false;
    }

  }
}
