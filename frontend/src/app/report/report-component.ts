import { Component, OnInit } from '@angular/core';
import { ReportComponentService } from '../crud/shared/report-component.service';
import { Router } from '@angular/router';

//const URL = 'http://localhost:8080/magazine';
 
@Component({
  selector: 'report-demo',
  templateUrl: './report-component.html'
})
export class ReportComponent implements OnInit {

    constructor(private httpService: ReportComponentService,private router:Router){
  
  
    }

    ngOnInit(){
   
        this.httpService.getReports();
    }

    // getReports(){
    //     this.httpService.getReports();
    // }

}