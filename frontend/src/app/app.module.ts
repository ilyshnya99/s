import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule, ToastrService } from 'ngx-toastr';
//import {LoginComponent} from './login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { httpInterceptorProviders } from './auth/auth-interceptor';
import { AddComponentComponent } from './crud/add-component/add-component.component';
import { EditComponentComponent } from './crud/edit-component/edit-component.component';
import { TableComponent } from './crud/table/table.component';
import { EmployeeComponentService } from './crud/shared/employee-component.service';

import { AlertsModule, AlertsService } from 'angular-alert-module';
import { from } from 'rxjs';
import { UploadComponent } from './crud/upload/upload.component';
import { CookieService } from 'ngx-cookie-service';
import { SimpleDemoComponent } from './file-upload.component/file-upload.component';
import { FileUploadModule } from 'ng2-file-upload';
import { ReportComponent } from './report/report-component';
import { ReportComponentService } from './crud/shared/report-component.service';
import { KeycloakService } from './auth/keycloack/keycloack.service';
import { CabinetComponent } from './cabinet/capinet.component';
import { User } from './cabinet/user';
import { NgxPaginationModule } from 'ngx-pagination';
import { FlagComponent } from './flag/flag-component';





export function kcFactory(keycloakService: KeycloakService) {
  return () => keycloakService.init();
}

@NgModule({
  declarations: [
    
    AppComponent,
UploadComponent,
    AddComponentComponent,
    EditComponentComponent,
    //CookieConsent,
    TableComponent,
    SimpleDemoComponent,
    ReportComponent, 
    CabinetComponent,
    FlagComponent
   
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FileUploadModule,
    NgxPaginationModule,
   
    
    
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      progressBar: true
    }),
    FormsModule,
    AlertsModule,
  ],
  providers:  [httpInterceptorProviders,
              EmployeeComponentService,
              ReportComponentService,
              AlertsService,
              ToastrService,
              CookieService,
              {
                provide: APP_INITIALIZER,
                useFactory: kcFactory,
                deps: [KeycloakService],
                multi: true
              }
              
              ],
  bootstrap: [AppComponent]
})
export class AppModule { }
