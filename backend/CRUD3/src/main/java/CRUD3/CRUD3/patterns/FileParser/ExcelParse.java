package CRUD3.CRUD3.patterns.FileParser;

import CRUD3.CRUD3.patterns.FileParser.reader.ExcelReader;
import CRUD3.CRUD3.services.EmployeeService;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ExcelParse extends TemplateMethod {


    public ExcelParse(ExcelReader reader,EmployeeService employeeService) {
        super(reader,employeeService);
    }

    public ExcelParse(){
        super();
    }

    @Override
    public boolean valid() {
        return this.getReader().getFile().getOriginalFilename().endsWith(".xls");
    }

    @Override
    public void parse() throws IOException {
        this.getReader().read(this.getEmployeeService());
    }

}
