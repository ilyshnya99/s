package CRUD3.CRUD3.patterns.BuilderReport;

import CRUD3.CRUD3.model.Report;

import java.time.LocalDate;

public class ReportFailedBuilder implements IReportBulder {

    private Report report;
    @Override
    public void setDate(LocalDate date) {
        report.setDate(date);
    }

    @Override
    public void setDuplicate(int duplicate) {
        report.setDuplicate(duplicate);
    }

    @Override
    public void setSuccessful(int successful) {
        report.setSuccessful(successful);
    }

    @Override
    public Report getResult() {
        return report;
    }

    public ReportFailedBuilder(int dupl,int succ){
        report=new Report();
        report.setStatus(Report.Status.FAILED);
        setDuplicate(dupl);
        setSuccessful(succ);
        setDate(LocalDate.now());
    }
}
