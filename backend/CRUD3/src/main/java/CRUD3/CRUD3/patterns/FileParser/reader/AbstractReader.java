package CRUD3.CRUD3.patterns.FileParser.reader;

import CRUD3.CRUD3.services.EmployeeService;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Data
public abstract class AbstractReader implements IReader{
    private String path;
    private MultipartFile file;
   // private EmployeeService employeeService;
    private int rows;

    public AbstractReader(String path,MultipartFile file){
        this.path=path;
        this.file=file;
    }

    @Override
    public abstract int getCount();

    @Override
    public abstract void read(EmployeeService employeeService) throws IOException ;
}
