package CRUD3.CRUD3.patterns.FileParser.reader;

import CRUD3.CRUD3.services.EmployeeService;

import java.io.IOException;

public interface IReader {

    int getCount();
    void read(EmployeeService employeeService) throws IOException;

}

