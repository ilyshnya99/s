package CRUD3.CRUD3.patterns.FileParser.reader;

import CRUD3.CRUD3.model.Employee;
import CRUD3.CRUD3.services.EmployeeService;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;

public class ExcelReader extends AbstractReader {

    private int count;

    public ExcelReader(String path, MultipartFile file){
        super(path,file);
    }


    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void read(EmployeeService employeeService) throws IOException {

        try (FileInputStream inputStream = new FileInputStream(this.getPath())) {
            // Get the workbook instance for XLS file
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            // Get first sheet from the workbook
            HSSFSheet sheet = workbook.getSheetAt(0);
            count=sheet.getLastRowNum();
            Employee employee = new Employee();
            for (int i = 1; i < sheet.getLastRowNum() + 1; i++) {
                employee.setId((long) sheet.getRow(i).getCell(0).getNumericCellValue());
                employee.setName(sheet.getRow(i).getCell(1).getStringCellValue());
                employee.setSurname(sheet.getRow(i).getCell(2).getStringCellValue());
                employee.setPatronymic(sheet.getRow(i).getCell(3).getStringCellValue());
                employee.setStatus(sheet.getRow(i).getCell(4).getStringCellValue());
                employee.setPosition(sheet.getRow(i).getCell(5).getStringCellValue());
                employee.setPhotoPath(sheet.getRow(i).getCell(6).getStringCellValue());
               employeeService.addEmployee(employee);
            }
        }
    }

}
