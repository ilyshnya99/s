package CRUD3.CRUD3.patterns.FileParser;


import CRUD3.CRUD3.patterns.FileParser.reader.AbstractReader;
import CRUD3.CRUD3.services.EmployeeService;
import lombok.Data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Data
abstract public class TemplateMethod {

    private AbstractReader reader;
    private String path;

    private EmployeeService employeeService;

    public TemplateMethod(AbstractReader reader,EmployeeService employeeService){
        this.employeeService=employeeService;
        this.reader=reader;
    }

    public TemplateMethod(){

    }

    private void upload() throws IOException {
        File convertFile = new File(reader.getPath());
        convertFile.createNewFile();
        try (FileOutputStream flout = new FileOutputStream(convertFile)) {
            flout.write(reader.getFile().getBytes());
        }
    }

    public abstract boolean valid();

    public abstract void parse() throws IOException;

    public int duplicate(int count){
        return this.reader.getCount()+count-this.getEmployeeService().count();
    }

    public int doMethod(int firstCount) throws IOException {
        if (valid()) {
            upload();
            parse();
            return duplicate(firstCount);
        }
        throw new IllegalArgumentException("нужен файл .xls");
    }


}
