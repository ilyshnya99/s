package CRUD3.CRUD3.patterns.BuilderReport;

import CRUD3.CRUD3.model.Report;

import java.time.LocalDate;

public interface IReportBulder {

    void setDate(LocalDate date);
    void setDuplicate(int duplicate);
    void setSuccessful(int successful);
    Report getResult();
}
