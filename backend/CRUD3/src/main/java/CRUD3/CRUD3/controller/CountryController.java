package CRUD3.CRUD3.controller;

import CRUD3.CRUD3.model.Country;
import CRUD3.CRUD3.model.Employee;
import CRUD3.CRUD3.services.CountryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/country")
public class CountryController {

    @Autowired
    private CountryService countryService;

    private final RestTemplate restTemplate;

    public CountryController(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @GetMapping("/all")
    public List<Country> getCountry() {
        if ( countryService.getCountries().size() == 0){
            var countries=(restTemplate.getForObject("https://restcountries.eu/rest/v2/all", Country[].class));
            List<Country> list=List.of(countries);
            return countryService.addAll(list);
        }else
            return countryService.getCountries();
    }


    @GetMapping("/name/{name}")
    public List<Country> getCountryByName(@PathVariable(value = "name") String field) {
        var c = restTemplate.getForObject("https://restcountries.eu/rest/v2/name/"+field, Country[].class);
        return List.of(c);

    }
}
