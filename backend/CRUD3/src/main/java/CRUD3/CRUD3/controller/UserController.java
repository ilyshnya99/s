package CRUD3.CRUD3.controller;


import CRUD3.CRUD3.model.User;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.jose.jwk.JWK;
import org.keycloak.representations.AccessToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping("/cabinet")
    @ResponseBody
    public User getProduct(KeycloakAuthenticationToken principal) {

        AccessToken token ;
        token=principal.getAccount().getKeycloakSecurityContext().getToken();

        User user=new User();
        user.setId( token.getId());
        user.setFirstName(token.getGivenName());
        user.setLastName(token.getFamilyName());
        user.setUserName(token.getPreferredUsername());
        user.setEmail(token.getEmail());
        user.setPicture(token.getPicture());
        return user;
    }



}
