package CRUD3.CRUD3.controller;

import CRUD3.CRUD3.model.Report;
import CRUD3.CRUD3.patterns.BuilderReport.ReportFailedBuilder;
import CRUD3.CRUD3.patterns.BuilderReport.ReportSuccessBuilder;
import CRUD3.CRUD3.patterns.FileParser.ExcelParse;
import CRUD3.CRUD3.patterns.FileParser.TemplateMethod;
import CRUD3.CRUD3.patterns.FileParser.reader.ExcelReader;
import CRUD3.CRUD3.services.EmployeeService;
import CRUD3.CRUD3.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;

@RestController
@RequestMapping("/file")
public class FileUpload {

    @Value("${filepath}")
    private String path;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    ReportService reportService;

 //   @PreAuthorize("hasRole('PM') or hasRole('ADMIN')")

    @RequestMapping(value="/upload")
    public ResponseEntity<Object> uploadFile(@RequestParam("file") MultipartFile file){
        //String s=request.getUserPrincipal().getName();
        if (file != null) {
           // request.getUserPrincipal().getName();
           // AccessToken token =principal.getAccount().getKeycloakSecurityContext().getToken();
            int dupl;
            var excel = new ExcelReader(path + file.getOriginalFilename(), file);
            TemplateMethod method = new ExcelParse(excel, employeeService);
            try {
                dupl = method.doMethod(employeeService.count());
            } catch (Exception e) {
                reportService.add(new ReportFailedBuilder(0,0).getResult());
                return new ResponseEntity<>("gg karoche", HttpStatus.NO_CONTENT);
            }
            reportService.add(new ReportSuccessBuilder(dupl,excel.getCount()).getResult());
            return new ResponseEntity<>("Добавлено/обновлено объектов - " + excel.getCount()
                    + "\n" + "Количество дубликатов - " + dupl + "\n", HttpStatus.OK);
        }
        reportService.add(new ReportFailedBuilder(0,0).getResult());
        return new ResponseEntity<>("File not found", HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/magazine")
    @PreAuthorize("hasRole('PM') or hasRole('ADMIN')")
    public List<Report> magazine() throws IOException {

        return reportService.getReports();
    }


}
