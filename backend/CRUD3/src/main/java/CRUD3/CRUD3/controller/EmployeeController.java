package CRUD3.CRUD3.controller;


import CRUD3.CRUD3.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import CRUD3.CRUD3.services.EmployeeService;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@CrossOrigin(origins = "")
//@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
@RestController
@RequestMapping("/api")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;


    @RequestMapping("/getEmployees")
    public List<Employee> getEmployees() {

        return employeeService.getEmployees();
    }

  //  @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @PostMapping( "/createEmployee")
    public Employee createEmployee(@Valid @RequestBody Employee employee) {
        return employeeService.addEmployee(employee);
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<Employee>  updateEmployee(@PathVariable(value =  "id") Long employeeId,
                                                   @Valid @RequestBody Employee employeeDetails) {
        Employee employee =  employeeService.getEmployee(employeeId);

        employee.setName(employeeDetails.getName());
        employee.setSurname(employeeDetails.getSurname());
        employee.setPatronymic(employeeDetails.getPatronymic());
        employee.setPhotoPath(employeeDetails.getPhotoPath());
        employee.setStatus(employeeDetails.getStatus());
        Employee updatedEmployee =  employeeService.addEmployee(employee);

        return ResponseEntity.ok(updatedEmployee);
    }

    @DeleteMapping("/employees/{id}")
    public Map<String, Boolean> deleteEmployee(@PathVariable(value =  "id") Long employeeId) throws Exception {

        employeeService.deleteEmployee(employeeService.getEmployee(employeeId));
        Map<String, Boolean> response =  new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @RequestMapping("/getEmployeesOrderBy/{name}")
    public List<Employee> getEmployeesOrderBy(@PathVariable(value = "name") String field) {
        //String authHeader = request.getHeader("Order");
        return employeeService.getEmployeeOrderByName(field);
    }

}
