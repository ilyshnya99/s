package CRUD3.CRUD3.model;

import lombok.Data;

@Data
public class User {

    private String userName;
    public String id;
    public String firstName;
    public String lastName;
    public String picture;
    private String email;
}
