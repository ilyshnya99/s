package CRUD3.CRUD3.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="country")
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String capital;
    private int population;
    private String flag;
}
