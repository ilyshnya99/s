package CRUD3.CRUD3.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="report")
@Data
public class Report {

    public enum Status{
        SUSSES,FAILED
    }

    public Report(int duplicate,int successful) {
        this.duplicate=duplicate;
        this.successful=successful;
    }
    public Report(){}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    public LocalDate date;

    public  Status status;

    public int successful;

    public int duplicate;

}

