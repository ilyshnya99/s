package CRUD3.CRUD3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class Crud3Application extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Crud3Application.class);
	}

	public static void main(String[] args) {
		//System.getProperties().put( "proxySet", "true" );
		//System.getProperties().put( "socksProxyHost", "127.0.0.1" );
		//System.getProperties().put( "socksProxyPort", "9150" );
//		ApiContextInitializer.init();
//		TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
//		try {
//			telegramBotsApi.registerBot(new TestBot());
//		} catch (TelegramApiException e) {
//			e.printStackTrace();
//		}
		SpringApplication.run(Crud3Application.class, args);
	}
}
