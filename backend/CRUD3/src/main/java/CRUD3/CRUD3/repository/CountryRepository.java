package CRUD3.CRUD3.repository;

import CRUD3.CRUD3.model.Country;
import CRUD3.CRUD3.model.Report;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository  extends JpaRepository<Country,Long> {
}

