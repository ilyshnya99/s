package CRUD3.CRUD3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import CRUD3.CRUD3.model.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {
    @Query("SELECT u FROM Employee u ORDER BY name")
    List<Employee> findAllOrderByName( String field);
    @Query("SELECT count(u.id) FROM Employee u")
    int getCount();
}
